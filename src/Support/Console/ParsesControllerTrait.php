<?php

namespace Squibler\Artisan\Support\Console;

use Illuminate\Support\Str;

trait ParsesControllerTrait
{
    use ParsesNameTrait;

    protected function parseController($name)
    {
        $controller = $this->parseName($name);

        if (! Str::endsWith($controller, 'Controller')) {
            $controller .= 'Controller';
        }

        return $controller;
    }
}