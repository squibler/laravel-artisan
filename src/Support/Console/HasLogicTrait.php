<?php

namespace Squibler\Artisan\Support\Console;

use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Str;

trait HasLogicTrait
{
    public function handleLogic($model=null)
    {
        if ($this->hasOption('all') && $this->option('all')) {
            $this->input->setOption('logic', true);
        }

        if ($this->option('logic')) {
            $this->createLogic($model);
        }
    }


    protected function createLogic($model)
    {
        $name = Str::studly(class_basename($this->argument('name')));
        $options = [
            'name' => $name,
            '--model' => ($model)
        ];

        $this->call('make:logic', $options);
    }


    protected function getOptions()
    {
        return array_merge( parent::getOptions(), [
            ['logic', 'l', InputOption::VALUE_NONE, 'Generate a logic extention for the model']
        ]);
    }
}