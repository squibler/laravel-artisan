<?php

namespace Squibler\Artisan\Support\Console;

trait ParsesNameTrait
{
    protected function parseName( $name )
    {
        if (preg_match('([^A-Za-z0-9_/\\\\])', $name)) {
            throw new \InvalidArgumentException('Name contains invalid characters.');
        }

        return trim(str_replace('/', '\\', $name), '\\');
    }
}