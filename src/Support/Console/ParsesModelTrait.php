<?php

namespace Squibler\Artisan\Support\Console;

use Illuminate\Support\Str;

trait ParsesModelTrait
{
    use ParsesNameTrait;

    protected function parseModel($name)
    {
        $model = $this->parseName($name);

        if (! Str::startsWith($model, $rootNamespace = $this->laravel->getNamespace())) {
            $model = $rootNamespace.'Models\\'.$model;
        }

        return $model;
    }
}