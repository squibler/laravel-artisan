<?php

namespace Squibler\Artisan\Console\Commands;


use Symfony\Component\Console\Input\InputOption;
use Illuminate\Routing\Console\ControllerMakeCommand as LaravelControllerMakeCommand;
use Illuminate\Support\Str;
use Squibler\Artisan\Support\Console\HasLogicTrait;
use Squibler\Artisan\Support\Console\ParsesModelTrait;

class ControllerMakeCommand extends LaravelControllerMakeCommand
{
    use HasLogicTrait, ParsesModelTrait;

    public function handle()
    {
        parent::handle();
        $this->handleLogic();
    }
}
