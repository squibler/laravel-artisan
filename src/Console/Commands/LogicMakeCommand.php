<?php

namespace Squibler\Artisan\Console\Commands;


use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Squibler\Artisan\Support\Console\ParsesModelTrait;
use Squibler\Artisan\Support\Console\ParsesControllerTrait;
use File;

class LogicMakeCommand extends GeneratorCommand
{
    use ParsesModelTrait, ParsesControllerTrait {
        ParsesControllerTrait::parseName insteadof ParsesModelTrait;
    }

    protected $name = 'make:logic';
    protected $signature = 'make:logic {--a|all} {--m|model} {--c|controller} {--d|migration} {name}';
    protected $description = 'Make Some Logic - Creates Logic';
    protected $type = 'Logic';


    protected function buildClass($name)
    {
        $replace = [];
        $all = $this->option('all');
        $classname = $this->argument('name');

        if ( $all || $this->option('model') ) {
            $replace = $this->buildModel($classname, $replace);
        }

        if ( $all || $this->option('controller') ) {
            $replace = $this->buildController($classname, $replace);
        }

        if ( $all || $this->option('migration') ) {
            $replace = $this->buildMigration($classname, $replace);
        }

        return str_replace(
            array_keys($replace), array_values($replace), parent::buildClass($name)
        );
    }


    protected function buildModel($classname, array $replace)
    {
        $modelClass = $this->parseModel($classname);
        if (!@class_exists($modelClass)) {
            $this->call('make:model', ['name' => $modelClass]);
        } else {
            $this->info('Model ' .$modelClass. ' Exists');
        }

        return array_merge($replace, [
            'DummyFullModelClass' => $modelClass,
            'DummyModelClass' => class_basename($modelClass),
            'DummyModelVariable' => lcfirst(class_basename($modelClass)),
        ]);
    }

    protected function buildController($classname, array $replace)
    {
        $controllerClass = $this->parseController($classname);

        if (!@class_exists($controllerClass)) {
            $this->call('make:controller', [
                'name' => $controllerClass,
                '--api' => true
            ]);
        } else {
            $this->info('Controller ' .$controllerClass. ' Exists');
        }

        return $replace;
    }


    protected function buildMigration($classname, array $replace)
    {
        $controllerClass = $this->parseName($classname);

        $this->call('make:migration', [
            'name' => sprintf('CreateTable%s', $controllerClass),
            '--create' => str_plural(strtolower($classname))
        ]);

        return $replace;
    }


    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\\' .$this->type;
    }


    protected function getStub()
    {
        $stub = ($this->option('all') || $this->option('model') )
              ? __DIR__.'/stubs/logic_model.stub'
              : __DIR__.'/stubs/logic.stub';
        return $stub;
    }


    protected function getNameInput()
    {
        return parent::getNameInput() . $this->type ;
    }
}
