<?php

namespace Squibler\Artisan\Console\Commands;

use Illuminate\Foundation\Console\ModelMakeCommand as LaravelModelMakeCommand;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Str;
use Squibler\Artisan\Support\Console\HasLogicTrait;
use Squibler\Artisan\Support\Console\ParsesControllerTrait;

class ModelMakeCommand extends LaravelModelMakeCommand
{
    use HasLogicTrait;

    public function handle()
    {
        parent::handle();
        $this->handleLogic(true);
    }


    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Models';
    }
}
