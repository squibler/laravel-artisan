<?php

namespace Squibler\Artisan\Providers;

use Illuminate\Foundation\Console\PresetCommand;
use Squibler\Artisan\Console\Commands\SquibPresetMopCommand;
use Squibler\Artisan\Console\Artisan\Squib;
use Squibler\Artisan\Console\Commands\LogicMakeCommand;
use Illuminate\Support\ServiceProvider;

class ArtisanServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app['env'] === 'local') {
            $this->commands([
                LogicMakeCommand::class
            ]);

            $this->app->extend('command.model.make', function ($command, $app) {
                return new \Squibler\Artisan\Console\Commands\ModelMakeCommand($app['files']);
            });


            $this->app->extend('command.controller.make', function ($command, $app) {
                return new \Squibler\Artisan\Console\Commands\ControllerMakeCommand($app['files']);
            });

            // PresetCommand::macro('squib', function ($command) {
            //     Squib::install($command);
            // });
        }
    }
}
