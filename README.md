# Models, Controllers & Logic

For when you want models to be created in the `app/Models` directory by default.

- Modifies `make:model` to use the `app/Models` directory and `App\Models` namespace
- Updates the artisan `php artisan make:model ModelName` command into into directory with correct namespacing
- Updates `php artian make:controller Controller -m` command to use the updated model structure
- Adds a new Logic command `php artisan make:logic Name` to create Business logic classes

## Usage

## 1. Install as dev dependency using composer

```bash
composer require --dev squibler/laravel-artisan
```

## 2. Models and Controllers

Artisan usage does not change from the bundled Laravel commands, in fact they in this packages they are simply extended to alter default paths or add some new tricks

### New default path

For Models, a new base path is defined and namespaced so that Models are created in a dedicated app/Models directory instead of being created in the app root as is the default Laravel behaviour.

Controllers still use the default and in this way have not changed.

### New option

Both the `make:controller` and `make:model` artisan commands get a new `--logic` (or shorter `-l`) option.

This option tells the command to also create a Business Logic class along with the Model or the Controller being created.

## 3. Business Logic Classes (optional use)

Business Logic classes are created in a new `app/Logic` directory and may extend from Models

You can create logic classes by using: `php artisan make:logic Name [--options]`

### Usage:

`php artisan make:logic [options] Name`

### Available Options

```
--model[=Model] | -m [Model]    Create Model if it does not exist and
                                then extend the Logic from the model.
--controller | -c               Create an API Controller `NameController` for the logic
--migration | -d                Create a new migration at the same time
--all                           Do all of the above
```

The idea behind Logic classes is that they can be single use. They can extend Models or be standalone. They can use many resources as needed.

In general, Logic classes sit between Controllers and Models helping keep both cleaner, more readable, and more purposeful.



##### app/Http/Controllers/MyController.php

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MyController extends Controller
{
    public function umbrella( Request $request )
    {
        // No Business Logic here only relationships
        return ShouldTakeUmbrellaLogic::byRequest($request);
    }
}
```

##### app/Logic/UmbrellaLogic.php

```php
<?php

namespace App\Logic;

use Illuminate\Http\Request;
use App\Models\Umbrella;

class ShouldTakeUmbrellaLogic
{
    public static function byRequest( Request $request )
    {
        if (! $address = $request->address) {
            $user = $request->user();
            $address = $user->address;
        }

        $forecast = WeatherForecastLogic::byAddress($address);
        return ($forecast->rain);
    }
}
```
